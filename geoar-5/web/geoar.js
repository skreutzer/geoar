/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    // TODO
}

let positions = Array();

function initialize()
{
    let result = 0;

    if ("geolocation" in navigator)
    {
        navigator.geolocation.getCurrentPosition(positionRetrieved, positionRetrievalError);
        navigator.geolocation.watchPosition(positionRetrieved, positionRetrievalError);
    }
    else
    {
        alert("Could not find geolocation in navigator.");
        console.log("Could not find geolocation in navigator.");
        result += -1;
    }

    if ("DeviceOrientationEvent" in window)
    {
        /** @todo https://w3c.github.io/deviceorientation/#id=permission-model probably
          * in effect on iOS request with DeviceOrientationEvent.requestPermission(). */

        window.addEventListener("deviceorientationabsolute", orientationRetrieved);
    }
    else
    {
        alert("Could not find DeviceOrientationEvent in window.");
        console.log("Could not find DeviceOrientationEvent in window.");
        result += -2;
    }

    return result;
}

function positionRetrieved(position)
{
    positions = Array();

    let target = document.getElementById("positions");

    if (target != null)
    {
        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }

    target = document.getElementById("location");

    if (target != null)
    {
        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }

        {
            let time = new Date;

            time = String(time.getUTCHours()).padStart(2, '0') + ":" +
                   String(time.getUTCMinutes()).padStart(2, '0') + ":" +
                   String(time.getUTCSeconds()).padStart(2, '0') + "Z";

            let text = document.createTextNode(position.coords.latitude + "," +
                                               position.coords.longitude + "; " +
                                               parseFloat(position.coords.accuracy).toFixed(1) + "m; " +
                                               time);
            target.appendChild(text);
        }
    }

    /*
    let accuracyMeterOffset = position.coords.accuracy / 2.0;

    let latitudeOffset = accuracyMeterOffset / 111111.0;
    let latitudeNegative = position.coords.latitude - latitudeOffset;
    let latitudePositive = position.coords.latitude + latitudeOffset;

    // Latitude is in degrees, Math.cos() expects radians.
    let latitudeRadians = position.coords.latitude * (Math.PI / 180.0);
    let cosineLatitude = Math.cos(latitudeRadians);
    let longitudeOffset = accuracyMeterOffset / (111111.0 * cosineLatitude);
    let longitudeNegative = position.coords.longitude - longitudeOffset;
    let longitudePositive = position.coords.longitude + longitudeOffset;
    */

    if (xmlhttp != null)
    {
        let requestPath = "";

        {
            let scripts = document.getElementsByTagName("script");
            let scriptPath = null;

            for (let i = scripts.length-1; i >= 0; i--)
            {
                if (!("src" in scripts.item(i)))
                {
                    continue;
                }

                if (scripts.item(i).src === undefined ||
                    scripts.item(i).src === null ||
                    scripts.item(i).src == "")
                {
                    continue;
                }

                scriptPath = scripts.item(i).src;
                break;
            }

            if (scriptPath !== null)
            {
                requestPath += scriptPath.substring(0, scriptPath.lastIndexOf('/'));
                requestPath += "/";
            }

            requestPath += "get.php";
        }

        // 'file://' is bad.
        if (requestPath.substring(0, 7) == "file://")
        {
            requestPath = requestPath.substr(8);
            // TODO: https?
            requestPath = "https://" + requestPath;
        }

        xmlhttp.open('GET', requestPath + "?latitude=" + position.coords.latitude + "&longitude=" + position.coords.longitude + "&accuracy=" + position.coords.accuracy, true);
        xmlhttp.onreadystatechange = function() { requestSubmitted(position.coords.latitude, position.coords.longitude); };
        xmlhttp.send();
    }
    else
    {
        // TODO
    }
}

function requestSubmitted(centerLatitude, centerLongitude)
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        positions = Array();

        if (xmlhttp.responseXML == null)
        {
            return;
        }

        {
            let target = document.getElementById("positions");

            if (target != null)
            {
                let positionsSource = xmlhttp.responseXML.getElementsByTagName("position");

                if (positionsSource != null)
                {
                    for (let i = 0, max = positionsSource.length; i < max; i++)
                    {
                        let positionSource = positionsSource.item(i);

                        if (positionSource.hasAttribute("name") != true)
                        {
                            throw "\"position\" element is missing its \"name\" attribute.";
                        }

                        if (positionSource.hasAttribute("latitude") != true)
                        {
                            throw "\"position\" element is missing its \"latitude\" attribute.";
                        }

                        if (positionSource.hasAttribute("longitude") != true)
                        {
                            throw "\"position\" element is missing its \"longitude\" attribute.";
                        }

                        let pointLatitude = parseFloat(positionSource.getAttribute("latitude"));

                        if (isNaN(pointLatitude) == true)
                        {
                            console.log("\"latitude\" is not a number!");
                            continue;
                        }

                        let pointLongitude = parseFloat(positionSource.getAttribute("longitude"));

                        if (isNaN(pointLongitude) == true)
                        {
                            console.log("\"longitude\" is not a number!");
                            continue;
                        }

                        let degrees = null;
                        let distance = null;
                        let distanceMeters = null;

                        if (pointLatitude == centerLatitude &&
                            pointLongitude == centerLongitude)
                        {
                            degrees = 0.0;
                            distance = 0.0;
                            distanceMeters = 0.0;
                        }
                        else
                        {
                            if (pointLongitude >= centerLongitude)
                            {
                                // Eastern (0° to <= 180°)

                                if (pointLatitude > centerLatitude)
                                {
                                    // North to East (0° to < 90°)

                                    let oppositeLength = pointLatitude - centerLatitude;
                                    let adjacentLength = pointLongitude - centerLongitude;

                                    let radians = Math.atan(adjacentLength / oppositeLength);
                                    degrees = ((radians * 180.0) / Math.PI) + 0.0;

                                    distance = Math.sqrt(Math.pow(oppositeLength, 2) + Math.pow(adjacentLength, 2));
                                }
                                else
                                {
                                    // East to South (90° to < 180°)

                                    let oppositeLength = centerLatitude - pointLatitude;
                                    let adjacentLength = pointLongitude - centerLongitude;

                                    let radians = Math.atan(oppositeLength / adjacentLength);
                                    degrees = ((radians * 180.0) / Math.PI) + 90.0;

                                    distance = Math.sqrt(Math.pow(oppositeLength, 2) + Math.pow(adjacentLength, 2));
                                }
                            }
                            else
                            {
                                // Western (> 180° to < 360°)

                                if (pointLatitude < centerLatitude)
                                {
                                    // South to West (180° to < 270°)

                                    let oppositeLength = centerLatitude - pointLatitude;
                                    let adjacentLength = centerLongitude - pointLongitude;

                                    let radians = Math.atan(adjacentLength / oppositeLength);
                                    degrees = ((radians * 180.0) / Math.PI) + 180.0;

                                    distance = Math.sqrt(Math.pow(oppositeLength, 2) + Math.pow(adjacentLength, 2));
                                }
                                else
                                {
                                    // West to North (270° to < 360°)

                                    let oppositeLength = pointLatitude - centerLatitude;
                                    let adjacentLength = centerLongitude - pointLongitude;

                                    let radians = Math.atan(oppositeLength / adjacentLength);
                                    degrees = ((radians * 180.0) / Math.PI) + 270.0;

                                    distance = Math.sqrt(Math.pow(oppositeLength, 2) + Math.pow(adjacentLength, 2));
                                }
                            }
                        }

                        if (distanceMeters === null)
                        {
                            let piFactor = Math.PI / 180.0;
                            let earthMeanRadiusMeters = 6371000.0;

                            let tempResult = (0.5 - (Math.cos((pointLatitude - centerLatitude) * piFactor) / 2.0)) +
                                             (Math.cos(centerLatitude * piFactor) * 
                                              Math.cos(pointLatitude * piFactor) *
                                              ((1.0 - Math.cos((pointLongitude - centerLongitude) * piFactor)) / 2.0));

                            distanceMeters = (2.0 * earthMeanRadiusMeters) * Math.asin(Math.sqrt(tempResult));
                        }

                        if (degrees === null)
                        {
                            
                        }

                        if (distance === null)
                        {
                            
                        }

                        if (distanceMeters === null)
                        {
                            
                        }

                        let position = { name: positionSource.getAttribute("name"),
                                         angle: degrees,
                                         distance: distanceMeters };
                        positions.push(position);
                    }

                    target = document.getElementById("location");

                    if (target != null)
                    {
                        let text = document.createTextNode(": (" + positions.length + ")");
                        target.appendChild(text);
                    }
                }
            }
        }
    }
    else if (xmlhttp.readyState == 4 && xmlhttp.status == 0)
    {
        alert("Offline...");
    }
}

function positionRetrievalError(error)
{
    alert(error.message);
    console.log(error.message);
}

function orientationRetrieved(deviceOrientationEvent)
{
    let target = document.getElementById("direction");

    if (target != null)
    {
        if (deviceOrientationEvent.alpha !== null)
        {
            if (deviceOrientationEvent.absolute === true)
            {
                while (target.hasChildNodes() == true)
                {
                    target.removeChild(target.lastChild);
                }

                // 0° is north with device in portrait upward forward direction.
                // 360.0 - for inversion because degrees are counter-clockwise to the left.
                /** @todo Add support for landscape orientation. */
                let viewAngle = parseFloat(360.0 - deviceOrientationEvent.alpha);

                let text = document.createTextNode(viewAngle.toFixed(0) + "°");
                target.appendChild(text);

                let viewAngleLowerBound = viewAngle - 20.0;
                let viewAngleUpperBound = viewAngle + 20.0;

                if (viewAngleLowerBound < 0.0)
                {
                    viewAngleLowerBound = 360.0 - (viewAngleLowerBound * -1.0);
                }

                if (viewAngleUpperBound >= 360.0)
                {
                    viewAngleUpperBound = viewAngleUpperBound - 360.0;
                }

                target = document.getElementById("positions");

                while (target.hasChildNodes() == true)
                {
                    target.removeChild(target.lastChild);
                }

                for (let i = 0, max = positions.length; i < max; i++)
                {
                    if (positions[i].distance <= 5.0 ||
                        (positions[i].angle >= viewAngleLowerBound &&
                         positions[i].angle <= viewAngleUpperBound))
                    {
                        let container = document.createElement("div");
                        container.setAttribute("class", "position");

                        let text = document.createTextNode(positions[i].name + ", " + parseFloat(positions[i].distance).toFixed(1) + "m");

                        container.appendChild(text);
                        target.appendChild(container);
                    }
                }
            }
        }
        else
        {
            while (target.hasChildNodes() == true)
            {
                target.removeChild(target.lastChild);
            }

            if (deviceOrientationEvent.beta === null && deviceOrientationEvent.gamma === null)
            {
                alert("No device orientation support.");
                console.log("No device orientation support.");
            }
        }
    }

    return 0;
}
