<?php
/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @todo GPS position might get reported way too frequently from the JavaScript
 *     event, leading to many requests and database lookups. Calculate (accumulated)
 *     distance since the last event and/or have a timer?
 * @todo Installation doesn't delete itself (or maybe deactivate it by check for
 *     presence of ./libraries/database_connect.inc.php?).
 */



require_once(dirname(__FILE__)."/libraries/https.inc.php");

if (isset($_GET['latitude']) !== true ||
    isset($_GET['longitude']) !== true ||
    isset($_GET['accuracy']) !== true)
{
    http_response_code(400);
    exit(0);
}

/** @todo $_GET: Sanity checks, typecast to double. */

require_once(dirname(__FILE__)."/libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(1);
}

/** @todo Loss of precision? */
$latitude = (double)$_GET['latitude'];
$longitude = (double)$_GET['longitude'];
/** @todo Maybe reject requests with bad accuracy, server- and/or client-side? */
$accuracy = (double)$_GET['accuracy'];

$diameterMeters = 200.0;
// Radius:
$offsetMeters = $diameterMeters / 2.0;

$latitudeOffset = $offsetMeters / 111111.0;
$latitudeNegative = $latitude - $latitudeOffset;
$latitudePositive = $latitude + $latitudeOffset;

// Latitude is in degrees, cos() expects radians. There's also deg2rad().
$latitudeRadians = $latitude * (M_PI / 180.0);
$cosineLatitude = cos($latitudeRadians);
$longitudeOffset = $offsetMeters / (111111.0 * $cosineLatitude);
$longitudeNegative = $longitude - $longitudeOffset;
$longitudePositive = $longitude + $longitudeOffset;

// Querying candidates per rectangle.
$positions = Database::Get()->Query("SELECT `id`,\n".
                                    "    `name`,\n".
                                    "    `latitude`,\n".
                                    "    `longitude`\n".
                                    "FROM `".Database::Get()->GetPrefix()."position`\n".
                                    "WHERE (`latitude`>=? AND `latitude`<=?) AND\n".
                                    "    (`longitude`>=? AND `longitude`<=?)",
                                    array($latitudeNegative, $latitudePositive, $longitudeNegative, $longitudePositive),
                                    array(Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING));

if (is_array($positions) !== true)
{
    http_response_code(500);
    exit(1);
}

// Remove positions within the query rectangle, but outside the circle area.
for ($i = 0, $max = count($positions); $i < $max; $i++)
{
    $x = (double)$positions[$i]['latitude'];
    $y = (double)$positions[$i]['longitude'];

    // < within, == on, > outside.
    if ((pow(($x - $latitude), 2) + pow(($y - $longitude), 2)) > pow($latitudeOffset, 2))
    {
        unset($positions[$i]);
    }
}

$positions = array_values($positions);

header("Content-Type: application/xml");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
     "<positions>";

for ($i = 0, $max = count($positions); $i < $max; $i++)
{
    echo "<position name=\"".htmlspecialchars($positions[$i]['name'], ENT_XML1 | ENT_QUOTES, "UTF-8")."\" latitude=\"".((double)$positions[$i]['latitude'])."\" longitude=\"".((double)$positions[$i]['longitude'])."\"/>";
}

echo "</positions>";


?>
