<?php
/* Copyright (C) 2017-2021 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

require_once(dirname(__FILE__)."/libraries/https.inc.php");

require_once(dirname(__FILE__)."/libraries/languagelib.inc.php");
require_once(getLanguageFile("index.webmanifest"));

echo "{".
       "\"name\": ".json_encode(LANG_NAME).",".
       "\"short_name\": ".json_encode(LANG_SHORTNAME).",".
       "\"start_url\": \"./index.php?lang=".getCurrentLanguage()."\",".
       "\"scope\": \"./index.php\",".
       // "standalone" just because Google doesn't support "browser".
       "\"display\": \"standalone\",".
       "\"background_color\": \"#000000\",".
       "\"description\": ".json_encode(LANG_DESCRIPTION).",".
       "\"icons\":".
       "[".
         "{".
           "\"src\": \"launcher-icon-1x.png\",".
           "\"sizes\": \"48x48\",".
           "\"type\": \"image/png\"".
         "},".
         "{".
           "\"src\": \"launcher-icon-2x.png\",".
           "\"sizes\": \"96x96\",".
           "\"type\": \"image/png\"".
         "},".
         "{".
           "\"src\": \"launcher-icon-4x.png\",".
           "\"sizes\": \"192x192\",".
           "\"type\": \"image/png\"".
         "}".
       "],".
       "\"related_applications\":".
       "[".
         "{".
           "\"platform\": \"web\",".
           "\"url\": ";

if (HTTPS_ENABLED === true)
{
    echo "\"https://".$_SERVER['HTTP_HOST']."?lang=".getCurrentLanguage()."\"";
}
else
{
    echo "\"http://".$_SERVER['HTTP_HOST']."?lang=".getCurrentLanguage()."\"";
}

echo     "}".
       "]".
     "}".
     "\n";

?>
