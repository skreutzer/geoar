/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function initialize()
{
    if ("geolocation" in navigator)
    {
        navigator.geolocation.getCurrentPosition(positionRetrieved, positionRetrievalError);
    }
    else
    {
        console.log("Could not find geolocation in navigator.");
        return -1;
    }

    return 0;
}

function positionRetrieved(position)
{
    alert(position.coords.latitude + ", " + position.coords.longitude);
    console.log(position.coords.latitude, position.coords.longitude);
}

function positionRetrievalError(error)
{
    alert(error.message);
    console.log(error.message);
}
