<?php
/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

require_once(dirname(__FILE__)."/libraries/https.inc.php");

require_once(dirname(__FILE__)."/libraries/languagelib.inc.php");
require_once(getLanguageFile("index"));

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"icon\" href=\"./favicon.png\" type=\"image/png\"/>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <link rel=\"manifest\" href=\"./index.webmanifest.php?lang=".getCurrentLanguage()."\"/>\n".
     "    <meta http-equiv=\"expires\" content=\"1296000\"/>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "    <script type=\"text/javascript\" src=\"geoar.js\"></script>\n".
     "    <script type=\"text/javascript\" src=\"serviceWorker.js\"></script>\n".
     "    <script type=\"text/javascript\">\n".
     "\n".
     "      \"use strict\";\n".
     "\n".
     "      function registerServiceWorker()\n".
     "      {\n".
     "          if (\"serviceWorker\" in navigator)\n".
     "          {\n".
     "              navigator.serviceWorker.register('serviceWorker.js')\n".
     "                .then(function(reg) {\n".
     "                    console.log(\"Service worker registered.\");\n".
     "                })\n".
     "                .catch(function(err) {\n".
     "                    console.log(err);\n".
     "                });\n".
     "          }\n".
     "          else\n".
     "          {\n".
     "              console.log(\"Could not find serviceWorker in navigator.\");\n".
     "          }\n".
     "      }\n".
     "\n".
     "      registerServiceWorker();\n".
     "\n".
     "      window.onload = function() {\n".
     "          initialize();\n".
     "      };\n".
     "    </script>\n".
     "  </head>\n".
     "  <body>\n";



echo "  </body>\n".
     "</html>\n".
     "\n";



?>
