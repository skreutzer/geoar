/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    // TODO
}

function initialize()
{
    if ("geolocation" in navigator)
    {
        navigator.geolocation.getCurrentPosition(positionRetrieved, positionRetrievalError);
        navigator.geolocation.watchPosition(positionRetrieved, positionRetrievalError);
    }
    else
    {
        alert("Could not find geolocation in navigator.");
        console.log("Could not find geolocation in navigator.");
        return -1;
    }

    return 0;
}

function positionRetrieved(position)
{
    {
        let target = document.getElementById("positions");

        if (target != null)
        {
            let container = document.createElement("div");
            container.setAttribute("class", "position");

            let text = document.createTextNode(position.timestamp + ": " + position.coords.latitude + ", " + position.coords.longitude + " (" + position.coords.accuracy + ")");

            container.appendChild(text);
            target.appendChild(container);
        }
    }

    if (xmlhttp != null)
    {
        let requestPath = "";

        {
            let scripts = document.getElementsByTagName("script");
            let scriptPath = null;

            for (let i = scripts.length-1; i >= 0; i--)
            {
                if (!("src" in scripts.item(i)))
                {
                    continue;
                }

                if (scripts.item(i).src === undefined ||
                    scripts.item(i).src === null ||
                    scripts.item(i).src == "")
                {
                    continue;
                }

                scriptPath = scripts.item(i).src;
                break;
            }

            if (scriptPath !== null)
            {
                requestPath += scriptPath.substring(0, scriptPath.lastIndexOf('/'));
                requestPath += "/";
            }

            requestPath += "position.php";
        }

        // 'file://' is bad.
        if (requestPath.substring(0, 7) == "file://")
        {
            requestPath = requestPath.substr(8);
            // TODO: https?
            requestPath = "https://" + requestPath;
        }

        // Cookie is submitted automatically.
        xmlhttp.open('PUT', requestPath, true);
        xmlhttp.onreadystatechange = positionSubmitted;
        xmlhttp.send(position.timestamp + "," + position.coords.latitude + "," + position.coords.longitude + "," + position.coords.accuracy);
    }
    else
    {
        // TODO
    }
}

function positionSubmitted()
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if (xmlhttp.responseText == '')
        {
            return;
        }
    }
    else if (xmlhttp.readyState == 4 && xmlhttp.status == 0)
    {
        alert("Offline...");
    }
}

function positionRetrievalError(error)
{
    alert(error.message);
    console.log(error.message);
}
