<?php
/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */



require_once(dirname(__FILE__)."/libraries/https.inc.php");
require_once(dirname(__FILE__)."/libraries/session.inc.php");

if (isset($_SESSION['user_token']) != true)
{
    $_SESSION['user_token'] = md5(uniqid(rand(), true));
}


$source = @fopen("php://input", "r");
$chunkWritten = false;

if ($source != false)
{
    $destination = @fopen("./positions/".$_SESSION['user_token'].".csv", "a");

    if ($destination != false)
    {
        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            @fwrite($destination, $chunk);
        }

        $chunkWritten = true;

        @fwrite($destination, "\r\n");

        @fclose($destination);
    }
    else
    {
        http_response_code(500);
    }

    @fclose($source);
}
else
{
    http_response_code(403);
}

if ($chunkWritten !== true)
{
    exit(-1);
}



?>
