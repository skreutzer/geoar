/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let position = null;
let direction = null;
let time = null;

function initialize()
{
    let result = 0;

    if ("geolocation" in navigator)
    {
        navigator.geolocation.getCurrentPosition(positionRetrieved, positionRetrievalError);
        navigator.geolocation.watchPosition(positionRetrieved, positionRetrievalError);
    }
    else
    {
        alert("Could not find geolocation in navigator.");
        console.log("Could not find geolocation in navigator.");
        result += -1;
    }

    if ("DeviceOrientationEvent" in window)
    {
        /** @todo https://w3c.github.io/deviceorientation/#id=permission-model probably
          * in effect on iOS request with DeviceOrientationEvent.requestPermission(). */
        /** @todo Might be deprecated in Chromium https://developer.chrome.com/blog/device-orientation-changes/
          * but not in/for Mozilla Firefox (MDN/W3C). */

        window.addEventListener("deviceorientationabsolute", orientationRetrieved);
    }
    else
    {
        alert("Could not find DeviceOrientationEvent in window.");
        console.log("Could not find DeviceOrientationEvent in window.");
        result += -2;
    }

    return result;
}

function positionRetrieved(pos)
{
    time = new Date;

    position = {
        "latitude": pos.coords.latitude,
        "longitude": pos.coords.longitude,
        "accuracy": parseFloat(pos.coords.accuracy).toFixed(1)
    };

    update();
}

function positionRetrievalError(error)
{
    alert(error.message);
    console.log(error.message);

    time = null;
}

function orientationRetrieved(deviceOrientationEvent)
{
    time = new Date;

    if (deviceOrientationEvent.alpha !== null)
    {
        if (deviceOrientationEvent.absolute === true)
        {
            // 0° is north with device in portrait upward forward direction.
            // 360.0 - for inversion because degrees are counter-clockwise to the left.
            /** @todo Add support for landscape orientation. */
            let viewAngle = parseFloat(360.0 - deviceOrientationEvent.alpha);
            direction = viewAngle.toFixed(0);
        }
        else
        {
            time = null;
        }
    }
    else
    {
        if (deviceOrientationEvent.beta === null && deviceOrientationEvent.gamma === null)
        {
            alert("No device orientation support.");
            console.log("No device orientation support.");
        }

        time = null;
    }

    return 0;
}

function update()
{
    let target = document.getElementById("location");

    if (target == null)
    {
        return -1;
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }

    if (time == null)
    {
        return 1;
    }

    let timeString = String(time.getUTCFullYear()) + "-" +
                     String(time.getUTCMonth() + 1).padStart(2, '0') + "-" +
                     String(time.getUTCDate()).padStart(2, '0') + "T" +
                     String(time.getUTCHours()).padStart(2, '0') + ":" +
                     String(time.getUTCMinutes()).padStart(2, '0') + ":" +
                     String(time.getUTCSeconds()).padStart(2, '0') + "Z";

    let positionString = "\"\"";

    if (position != null)
    {
        positionString = "\"" + position.latitude + "\"," +
                         "\"" + position.longitude + "\"," +
                         "\"" + position.accuracy + "\"";
    }

    let directionString = "\"\"";

    if (direction != null)
    {
        directionString = "\"" + direction + "\"";
    }

    let resultString = "\"" + timeString + "\"," + positionString + "," + directionString + "";
    target.appendChild(document.createTextNode(resultString));
    target.appendChild(document.createElement("br"));
}

/** @todo Add start/stop button to pause, to save battery.
  * Should unregister events and re-register these. */
/** @todo Add CSV to localStorage. */
/** @todo Button to export/save-as the CSV from localStorage. */
/** @todo Reset/clear localStorage/CSV/DOM? */

// "\"time\",\"latitude\",\"longitude\",\"accuracy\",direction\"\r\n"
