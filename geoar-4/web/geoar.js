/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    // TODO
}

function initialize()
{
    if ("geolocation" in navigator)
    {
        navigator.geolocation.getCurrentPosition(positionRetrieved, positionRetrievalError);
        navigator.geolocation.watchPosition(positionRetrieved, positionRetrievalError);
    }
    else
    {
        alert("Could not find geolocation in navigator.");
        console.log("Could not find geolocation in navigator.");
        return -1;
    }

    return 0;
}

function positionRetrieved(position)
{
    let accuracyMeterOffset = position.coords.accuracy / 2.0;

    let latitudeOffset = accuracyMeterOffset / 111111.0;
    let latitudeNegative = position.coords.latitude - latitudeOffset;
    let latitudePositive = position.coords.latitude + latitudeOffset;

    // Latitude is in degrees, Math.cos() expects radians.
    let latitudeRadians = position.coords.latitude * (Math.PI / 180.0);
    let cosineLatitude = Math.cos(latitudeRadians);
    let longitudeOffset = accuracyMeterOffset / (111111.0 * cosineLatitude);
    let longitudeNegative = position.coords.longitude - longitudeOffset;
    let longitudePositive = position.coords.longitude + longitudeOffset;

    let rectLocation = [
                         [ latitudeNegative, longitudeNegative ],
                         [ latitudePositive, longitudePositive ]
                       ];

    if (xmlhttp != null)
    {
        let requestPath = "";

        {
            let scripts = document.getElementsByTagName("script");
            let scriptPath = null;

            for (let i = scripts.length-1; i >= 0; i--)
            {
                if (!("src" in scripts.item(i)))
                {
                    continue;
                }

                if (scripts.item(i).src === undefined ||
                    scripts.item(i).src === null ||
                    scripts.item(i).src == "")
                {
                    continue;
                }

                scriptPath = scripts.item(i).src;
                break;
            }

            if (scriptPath !== null)
            {
                requestPath += scriptPath.substring(0, scriptPath.lastIndexOf('/'));
                requestPath += "/";
            }

            requestPath += "get.php";
        }

        // 'file://' is bad.
        if (requestPath.substring(0, 7) == "file://")
        {
            requestPath = requestPath.substr(8);
            // TODO: https?
            requestPath = "https://" + requestPath;
        }

        xmlhttp.open('GET', requestPath + "?latitudeNegative=" + rectLocation[0][0] + "&longitudeNegative=" + rectLocation[0][1] + "&latitudePositive=" + rectLocation[1][0] + "&longitudePositive=" + rectLocation[1][1], true);
        xmlhttp.onreadystatechange = requestSubmitted;
        xmlhttp.send();
    }
    else
    {
        // TODO
    }
}

function requestSubmitted()
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if (xmlhttp.responseXML == null)
        {
            return;
        }

        {
            let target = document.getElementById("positions");

            if (target != null)
            {
                while (target.hasChildNodes() == true)
                {
                    target.removeChild(target.lastChild);
                }

                let positions = xmlhttp.responseXML.getElementsByTagName("position");

                if (positions != null)
                {
                    for (let i = 0, max = positions.length; i < max; i++)
                    {
                        let position = positions.item(i);

                        if (position.hasAttribute("name") != true)
                        {
                            throw "\"position\" element is missing its \"name\" attribute.";
                        }

                        let container = document.createElement("div");
                        container.setAttribute("class", "position");

                        let text = document.createTextNode(position.getAttribute("name"));

                        container.appendChild(text);
                        target.appendChild(container);
                    }
                }
            }
        }
    }
    else if (xmlhttp.readyState == 4 && xmlhttp.status == 0)
    {
        alert("Offline...");
    }
}

function positionRetrievalError(error)
{
    alert(error.message);
    console.log(error.message);
}
