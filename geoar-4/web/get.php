<?php
/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @todo GPS position might get reported way too frequently from the JavaScript
 *     event, leading to many requests and database lookups. Calculate (accumulated)
 *     distance since the last event and/or have a timer?
 * @todo Installation doesn't delete itself (or maybe deactivate it by check for
 *     presence of ./libraries/database_connect.inc.php?).
 * @todo Allow user to expand the hitbox based on geolocation accuracy by a factor.
 * @todo Querying and hitbox testing should be changed to a circle with the
 *     radius based on the accuracy, also change in the JavaScript.
 */



require_once(dirname(__FILE__)."/libraries/https.inc.php");

if (isset($_GET['latitudeNegative']) !== true ||
    isset($_GET['longitudeNegative']) !== true ||
    isset($_GET['latitudePositive']) !== true ||
    isset($_GET['longitudePositive']) !== true)
{
    http_response_code(400);
    exit(0);
}

/** @todo $_GET: Sanity checks, invert depending equator for hitbox/range query, handle zero-overflow/-underflow, typecast to double. */

require_once(dirname(__FILE__)."/libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(1);
}

$positions = Database::Get()->Query("SELECT `id`,\n".
                                    "    `name`,\n".
                                    "    `latitude`,\n".
                                    "    `longitude`\n".
                                    "FROM `".Database::Get()->GetPrefix()."position`\n".
                                    "WHERE (`latitude`>=? AND `latitude`<=?) AND\n".
                                    "    (`longitude`>=? AND `longitude`<=?)",
                                    array($_GET['latitudeNegative'], $_GET['latitudePositive'], $_GET['longitudeNegative'], $_GET['longitudePositive']),
                                    array(Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING));

if (is_array($positions) !== true)
{
    http_response_code(500);
    exit(1);
}

header("Content-Type: application/xml");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
     "<positions>";

for ($i = 0, $max = count($positions); $i < $max; $i++)
{
    echo "<position name=\"".htmlspecialchars($positions[$i]['name'], ENT_XML1 | ENT_QUOTES, "UTF-8")."\"/>";
}

echo "</positions>";


?>
