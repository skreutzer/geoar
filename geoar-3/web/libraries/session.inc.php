<?php
/* Copyright (C) 2017-2020 Stephan Kreutzer
 *
 * This file is part of GeoAR.
 *
 * GeoAR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GeoAR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GeoAR. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/session.inc.php
 * @author Stephan Kreutzer
 * @since 2017-04-23
 */

if (@session_start() === true)
{
    if (empty($_SESSION) === true)
    {
        $_SESSION['instance_path'] = str_replace("\\", "/", dirname(__FILE__));
        $_SESSION['user_token'] = md5(uniqid(rand(), true));
    }
}
else
{
    http_response_code(403);
    exit(-1);
}

if (isset($_SESSION['instance_path']) !== true)
{
    http_response_code(500);
    exit(-1);
}

{
    $lhs = str_replace("\\", "/", dirname(__FILE__));

    if ($lhs !== $_SESSION['instance_path'])
    {
        http_response_code(403);
        exit(-1);
    }
}



?>
